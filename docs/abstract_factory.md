<div align="center">
  <h1>Abstract Factory</h1>
</div>

<div align="center">
  <img src="abstract_factory_icon.png" alt="drawing" width="250"/>
</div>

## Table of Contents

1. **[What is it?](#what-is-it)**
    1. [Real-World Analogy](#real-world-analogy)
    2. [Participants](#participants)
    3. [Collaborations](#collaborations)
2. **[When do you use it?](#when-do-you-use-it)**
    1. [Motivation](#motivation)
    2. [Known Uses](#known-uses)
    3. [Categorization](#categorization)
    4. [Aspects that can vary](#aspects-that-can-vary)
    5. [Solution to causes of redesign](#solution-to-causes-of-redesign)
    6. [Consequences](#consequences)
    7. [Relations with Other Patterns](#relations-with-other-patterns)
3. **[How do you apply it?](#how-do-you-apply-it)**
    1. [Structure](#structure)
    2. [Variations](#variations)
    3. [Implementation](#implementation)
4. **[Sources](#sources)**

<br>
<br>

## What is it?

> :large_blue_diamond: **Abstract Factory is a creational pattern to produce families of objects without specifying
their concrete classes.**

### Real-World Analogy

_A smartphone product line._

Different companies all have a similar product line (Abstract Factory) of smartphones (abstract products). For example
the Apple, Samsung, and Huawei have product lines (Concrete Factories) that offer various models (concrete products)
targeting
different market segments (clients) with their unique features and design.

### Participants

- :bust_in_silhouette: **AbstractFactory**
    - Declares an interface to:
        - `createProductA`: Create abstract product object.
        - `createProductB`: Create another abstract product object.
- :bust_in_silhouette: **AbstractProduct**
    - Declares an interface to:
        - `productMethodA`: methods of product object.
- :man: **ConcreteFactory**
    - Provides implementation for:
        - `createProductA`: Create concrete product object.
        - `createProductB`: Create another concrete product object.
- :man: **ConcreteProduct**
    - Provides implementation for:
        - `productMethodA`: methods of product object.

### Collaborations

The client only knows interfaces declared by **AbstractFactory** and **AbstractProduct** classes.

At run-time an instance of a **ConcreteFactory** class is created and passed to the client to make **ConcreteProducts**.

To create different **ConcreteProducts**, clients should use a different **ConcreteFactory**.


<br>
<br>

## When do you use it?

> :large_blue_diamond: **When a system should be configured with one of multiple product families. Or to enforce that a
family of classes is used together.**

### Motivation

- How can a client use objects without instantiating and relying on their concrete classes?
- How do we ensure there is only one place to modify when the constructor of used classes change?
- How do we enforce that a family of classes is used together?

### Known Uses

- UnitOfWork Factory:
    - Supporting different transaction isolation levels, managing different types of resources, or providing specialized
      behaviors for specific contexts (e.g., long-running transactions).
- Views Factory:
    - different types of views based on user roles or preferences, adapting the view creation process for different
      platforms (web, mobile), or supporting variations in themes or styles.
- ReadModelStores Factory:
    - creating different types of read model stores that are optimized for specific query patterns, data storage
      technologies (e.g., SQL databases, NoSQL databases), or caching strategies.
- Logger Factory:
    - Loggers that use various logging levels, formats, or destinations (file, database, external
      services).
- Transaction Factory:
    - supporting different transactional strategies (e.g., distributed transactions, compensating
      transactions)
- Repositories Factory:
    - creating repositories tailored for different data storage technologies (SQL, NoSQL), supporting various database
      providers (PostgreSQL, MySQL), or implementing specific data access patterns (e.g., repository per aggregate in
      Domain-Driven Design).
- Event Outbox Writer Factory:
    - Creating writers optimized for different event delivery mechanisms (e.g., Kafka, RabbitMQ, HTTP endpoints) or
      adapting to various messaging protocols or serialization formats.
- Generating look-and-feel specific user interface objects.
    - E.g: dark mode, light mode, modern, classic.
- Portability across different OSes
    - By providing concrete classes for each OS.
    - E.g: windows, linux, mac.
- File upload:
    - Create an upload strategy object for type of storage used by client, based on client ID.
    - E.g: Amazon S3, SFTP, WebDav
- Preview image generation:
    - Create a preview generator strategy object based on file MIME type.
    - E.g: document generator for pdf and doc, image generator for jpg and bmp.
- Cryptographic Libraries:
    - Producing various encryption and decryption algorithms based on security requirements or encryption standards.
- Game Development:
    - Creating different families of objects in games.
    - Such as different types of characters, weapons, or environments for various game levels.
- Testing:
    - Generate different test doubles based on the context that is required for testing.
- Creating parsers for different sources.
    - E.g: InputStream, File, URL.

### Categorization

Purpose:  **Creational**  
Scope:    **Object**   
Mechanisms: **Polymorphism**

Creational design patterns abstract the instantiation process.
They help make a system independent of how its objects are created, composed, and represented.
An object creational pattern will delegate instantiation to another object.

There are two recurring themes in these patterns.
First, they all encapsulate knowledge about which concrete classes the system uses.
Second, they hide how instances of these classes are created and put together.
All the system at large knows about the objects is their interfaces as defined by abstract classes.
Consequently, the creational patterns give you a lot of flexibility in what gets created, who creates it, how it gets
created, and when.

### Aspects that can vary

- Families of product objects.

### Solution to causes of redesign

- Creating an object by specifying a class explicitly.
    - Instantiating concrete classes directly in client code makes it harder to reuse and change.
- Dependence on object representations or implementations.
- Clients that know how an object is represented, stored, located, or implemented might need to be changed when the
  object changes.
- Dependence on hardware and software platform.
    - harder to port to other platforms.
    - may even be difficult to keep it up to date on its native platform.
- Tight coupling.
    - Hard to understand: a lot of context needs to be known to understand a part of the system.
    - Hard to change: changing one class necessitates changing many other classes.
    - Hard to reuse in isolation: because classes depend on each other.

### Consequences

| Advantages                                                                                                                                                                                                                                                                                        | Disadvantages                                                                                                                                                                                         | 
|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| :heavy_check_mark: **Isolates concrete classes.** <br> A factory isolates clients from implementation classes. Clients manipulate instances through their abstract interfaces. Product class names are isolated in the implementation of the concrete factory; they do not appear in client code. | :x: **Adding new products is difficult** <br> Supporting new kinds of products requires extending the factory interface, which involves changing the AbstractFactory class and all of its subclasses. |
| :heavy_check_mark: **Makes exchanging product families easy.** <br> The client can use different product configurations simply by changing the concrete factory. Because an abstract factory creates a complete family of products, the whole product family changes at once.                     | :x: **Increased complexity** <br> Applying the abstract factory pattern increases the number of classes in the system.                                                                                |
| :heavy_check_mark: **Promotes compatibility between products.** <br> When product objects in a family are designed to work together, it's important that an application use objects from only one family at a time. AbstractFactory makes this easy to enforce.                                   |                                                                                                                                                                                                       |

### Relations with Other Patterns

_Distinction from other patterns:_

- Factory Method creates different families of objects by subclassing while Abstract Factory does that through
  composition.

_Combination with other patterns:_

- AbstractFactory classes are often implemented with factory methods, but they can also be implemented using Prototype.
- A concrete factory is often a Singleton
- Factories are often employed to enforce the use of decorators, adapters, proxies and the bridge pattern.
    - We can wrap the subject with a proxy, adapter or decorator before returning it from the factory. The client never
      knows or cares that it’s using that instead of the real thing
    - For the bridge pattern the implementor is wrapped with the abstraction before being returned from the factory.

<br>
<br>

## How do you apply it?

> :large_blue_diamond: **Multiple factory classes with methods that each return product objects belonging to the same "
> family".**

### Structure

```mermaid
classDiagram
    direction LR
    namespace Factories {
        class AbstractFactory {
            <<interface>>
            + createProductA(): AbstractProductA*
            + createProductB(): AbstractProductB*
        }
        class ConcreteFactory1 {
            + createProductA(): ConcreteProductA1
            + createProductB(): ConcreteProductB1
        }
        class ConcreteFactory2 {
            + createProductA(): ConcreteProductA2
            + createProductB(): ConcreteProductB2
        }
    }

    AbstractFactory <|.. ConcreteFactory1: implements
    AbstractFactory <|.. ConcreteFactory2: implements

    namespace Products {
        class AbstractProductA {
            <<interface>>
        }
        class ConcreteProductA1
        class ConcreteProductA2
        class AbstractProductB {
            <<interface>>
        }
        class ConcreteProductB1
        class ConcreteProductB2
    }

    AbstractProductA <|.. ConcreteProductA1: implements
    AbstractProductA <|.. ConcreteProductA2: implements
    AbstractProductB <|.. ConcreteProductB1: implements
    AbstractProductB <|.. ConcreteProductB2: implements
```

```mermaid
classDiagram
    direction TB
    class ConcreteFactory1 {
        + createProductA(): ConcreteProductA1
        + createProductB(): ConcreteProductB1
    }
    class ConcreteFactory2 {
        + createProductA(): ConcreteProductA2
        + createProductB(): ConcreteProductB2
    }
    class ConcreteProductA1
    class ConcreteProductA2
    class ConcreteProductB1
    class ConcreteProductB2

    ConcreteFactory1 --> ConcreteProductA1: instantiates
    ConcreteFactory1 --> ConcreteProductB1: instantiates
    ConcreteFactory2 --> ConcreteProductA2: instantiates
    ConcreteFactory2 --> ConcreteProductB2: instantiates
```

### Variations

- **Prototype-based factory**: The concrete factory is initialized with a prototypical instance of each product, and
  creates
  a new product by cloning it.
    - :heavy_check_mark: Eliminates the need to have a new concrete factory class for each new product family.
    - :x: Requires clone operation on each product.
- **Class-based factory**: The concrete factory constructor is provided with the classes to instantiate for each
  product.
    - :heavy_check_mark: Eliminates the need to have a new concrete factory class for each new product family.
    - :x: Only available in languages that treat classes as first-class objects.
- **Parameterized factory**: Add a parameter to the create operation to determine the product to instantiate.
    - :heavy_check_mark: Eliminates the need to have a create method for each product.
    - :x: Less (type) safe

### Implementation

In the example we apply the abstract factory pattern in a todo app where you can choose different types of databases for
storage.

- [Interface (interface)]()
- [Concrete Oubject]()

The _"client"_ is a ... program:

- [Client]()

The unit tests exercise the public interface of ... :

- [Concrete Object test]()

<br>
<br>

## Sources

- [Design Patterns: Elements Reusable Object Oriented](https://www.amazon.com/Design-Patterns-Elements-Reusable-Object-Oriented/dp/0201633612)
- [Refactoring Guru - Abstract Factory](https://refactoring.guru/design-patterns/abstract-factory)
- [Head First Design Patterns: A Brain-Friendly Guide](https://www.amazon.com/Head-First-Design-Patterns-Brain-Friendly/dp/0596007124)
- [ArjanCodes: The Factory Pattern in Python // Separate Creation From Use](https://youtu.be/s_4ZrtQs8Do?si=E4XPpLnZWQZhyZuY)
- [ArjanCodes: Here’s a More Pythonic Factory Pattern](https://youtu.be/zGbPd4ZP39Y?si=-VkT96Adh4h4zKya)
- [CodeAesthetic: dependency injection, the best pattern](https://www.youtube.com/watch?v=J1f5b4vcxCQ)

<br>
<br>
